//
//  SecondViewController.swift
//  Homework 2
//
//  Created by Yuan on 1/31/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    // label of result to be displayed
    @IBOutlet weak var result: UILabel!
    
    // return to the previous screen
    @IBAction func return_t(_ sender: Any) {
        performSegue(withIdentifier: "backSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // display the output string
    override func viewDidAppear(_ animated: Bool) {
        result.text = display
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
