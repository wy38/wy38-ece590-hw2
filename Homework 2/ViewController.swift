//
//  ViewController.swift
//  Homework 2
//
//  Created by Yuan on 1/30/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

// global variable to store the output string
var display : String = ""

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.gender_t.delegate = self
        self.gender_t.dataSource = self
        self.name_t.delegate = self
        self.from_t.delegate = self
        self.degree_t.delegate = self
        self.programming_t.delegate = self
        self.hobbies_t.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    // Student Class
    class Student : CustomStringConvertible {
        //student's name
        var name : String = ""
        //student's gender
        var gender : String = ""
        // student's hometown
        var hometown : String = ""
        // degree working on currently
        var degree : String = ""
        // proficient programming languages
        var programming : String = ""
        // interests outside of school
        var interest : String = ""

        init(name : String, gender : String, hometown : String, degree : String, programming : String, interest : String) {
            self.name = name
            self.gender = gender
            self.hometown = hometown
            self.degree = degree
            self.programming = programming
            self.interest = interest
        }
        
        var description : String {
            if (gender == "Male") {
                return "\(self.name) is from \(self.hometown). He is working on \(self.degree) degree. He is proficient in programming languages such as \(self.programming). Additionally, he enjoys \(self.interest) outside of school."
            } else {
                return "\(self.name) is from \(self.hometown). She is working on \(self.degree) degree. She is proficient in programming languages such as \(self.programming). Additionally, she enjoys \(self.interest) outside of school."
            }
        }
    }
    
    // label of name
    @IBOutlet weak var name_t: UITextField!
    // label of hometown
    @IBOutlet weak var from_t: UITextField!
    // label of degree
    @IBOutlet weak var degree_t: UITextField!
    // label of programming languages
    @IBOutlet weak var programming_t: UITextField!
    // label of hobbies
    @IBOutlet weak var hobbies_t: UITextField!
    
    // Use picker view to select gender
    var gender_selected : String = ""
    @IBOutlet weak var gender_t: UIPickerView!
    
    let genders = ["Male", "Female"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        gender_selected = genders[row]
    }
    
    // Button to extract data and update the label text
    @IBAction func update(_ sender: UIButton) {
        let student_info = Student(name : name_t.text!, gender : gender_selected,
            hometown : from_t.text!,  degree : degree_t.text!, programming : programming_t.text!, interest : hobbies_t.text!)
        display = student_info.description
        performSegue(withIdentifier: "updateSegue", sender: self)
    }
    
    // Hide keyboard when user touch outside the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Hide keyboard when press return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBOutlet weak var image_t: UIImageView!
    
    // function to take the picture
    @IBAction func takePic(_ sender: UIButton) {
        let cam = UIImagePickerControllerSourceType.camera
        if (!UIImagePickerController.isSourceTypeAvailable(cam)) {
            print("no camera")
            return
        }
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerControllerSourceType.camera
        image.allowsEditing = false
        self.present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image_t.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
}


