Extra functions:

1. Use another control such as Picker View to select different gender. Hence, the output string can use "he" or "she" correctly.

2. Make it appear as 2 screens by using segue. The first screen is used to put in the information, and the second screen is used to display the output string.

3. Write functions to hide keyboard when user touches outside the keyboard or press the "return" button when testing with real ipod.

4. Add ability to take a picture and display it